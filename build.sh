#!/usr/bin/env bash
case $1 in
    win)
        echo 'Building for Windows' &&
        docker run -v "$(pwd):/src/" cdrx/pyinstaller-windows && cp -r src/resources dist/windows
        ;;
    *)
        echo 'Building for current machine' &&
        pyinstaller -D -F -n jumper -c "src/main.py" && cp -r src/resources dist
        ;;
esac
