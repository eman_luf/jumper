import pyglet
import sys
import os
from pyglet.window import key
from tree import Tree
from conf import SCALE

pyglet.resource.path = [os.path.join(os.path.dirname(sys.executable), 'resources')]
pyglet.resource.reindex()

scale = SCALE
width, height = int(scale * 1000), int(scale * 1000)
window = pyglet.window.Window(width, height)
pyglet.gl.glScalef(scale, scale, scale)

image = pyglet.resource.image('pp1.png')
image.anchor_x = image.width // 2
image.anchor_y = image.height // 2
image2 = image.get_transform(flip_x=True)

img_platform = pyglet.resource.image('platform.png')
img_platform.anchor_x = img_platform.width // 2
img_platform.anchor_y = img_platform.height // 2

score_label = pyglet.text.Label('0',
                                font_name='Times New Roman',
                                font_size=36,
                                x=0, y=0)

# TODO: Разобраться с рисованием пачками
batch = pyglet.graphics.Batch()

img_progress = pyglet.resource.image('progress_bar.png')
img_progress.anchor_x = img_progress.width
sprite_progress = pyglet.sprite.Sprite(img_progress)
sprite_progress.set_position((width - 40) // scale, 40 // scale)

img_progress_tile = pyglet.resource.image('progress_tile.png')

tiles_num = 6
sprite_progress_tiles = [pyglet.sprite.Sprite(img_progress_tile) for i in range(tiles_num)]
progress_space_x = (img_progress.width - img_progress_tile.width * tiles_num) / (tiles_num + 1)
progress_space_y = (img_progress.height - img_progress_tile.height) / 2

for i, sprite in enumerate(sprite_progress_tiles):
    pbx, pby = sprite_progress.position
    sprite.set_position(progress_space_x + (progress_space_x + img_progress_tile.width) * i - img_progress.width + pbx, progress_space_y + pby)


img_apple = pyglet.resource.image('apple1.png')
bg = pyglet.resource.image("spacecat2.jpg")
bg.height = height // scale
bg.width = width // scale


class Chara:
    def __init__(self):
        self.face = False
        self.imgs = [pyglet.sprite.Sprite(image), pyglet.sprite.Sprite(image2)]

    def get_img(self):
        if self.face:
            return self.imgs[1]
        return self.imgs[0]


def draw_node(node, x, y, sprites):
    if node:
        if node.heal and node != tree.curNode:
            heal_sprite = pyglet.sprite.Sprite(img_apple, batch=batch)
            heal_sprite.set_position(x, y + 20)
            sprites.append(heal_sprite)
        sprite = pyglet.sprite.Sprite(img_platform, batch=batch)
        sprite.set_position(x, y)
        sprites.append(sprite)
    if node.right:
        sprites = draw_node(node.right, x + img_platform.width, y + image.height // 2 + img_platform.height // 2, sprites)
    if node.left:
        sprites = draw_node(node.left, x - img_platform.width, y + image.height // 2 + img_platform.height // 2, sprites)
    return sprites


def draw_tree(tree, y):
    sprites = []
    x, y = width // (2 * scale), y - image.height // 2 - img_platform.height // 2
    return draw_node(tree.curNode, x, y, sprites)


@window.event
def on_draw():
    window.clear()
    bg.blit(0, 0)
    img = chara.get_img()
    y = 150
    img.set_position(width // (2 * scale), y)
    sprites = draw_tree(tree, y)
    img.draw()
    [sprite.draw() for sprite in sprites]
    score_label.text = str(tree.score)
    score_label.draw()
    sprite_progress.draw()
    [sprite.draw() for i, sprite in enumerate(sprite_progress_tiles) if i < time_left[0]]


@window.event
def on_key_press(symbol, modifiers):
    res = True
    scr = score[0]
    if symbol == key.LEFT:
        chara.face = False
        res, scr = tree.goLeft()
    elif symbol == key.RIGHT:
        chara.face = True
        res, scr = tree.goRight()
    if not res:
        print('You fall! Your score: %s' % scr)
        sys.exit()
    if tree.curNode.heal:
        time_left[0] += 2
        if time_left[0] > tiles_num:
            time_left[0] = tiles_num
    score[0] = scr


def time_lower(dt, time_left, score):
    time_left[0] -= 1
    if time_left[0] == -1:
        print("You didn't make it on time! Your score: %s" % score[0])
        sys.exit()


time_left = [tiles_num]
score = [0]

tree = Tree()
chara = Chara()
pyglet.clock.schedule_interval(time_lower, 1, time_left, score)

player = pyglet.media.Player()
music_source = pyglet.resource.media('LONGCAT BE LOOOOOOONG.mp3')
looper = pyglet.media.SourceGroup(music_source.audio_format, None)
looper.loop = True
looper.queue(music_source)
player.queue(looper)
player.play()

pyglet.app.run()
