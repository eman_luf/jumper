from random import choice, random


class Node:
    def __init__(self, parent_left=None, parent_right=None):
        self.right = None
        self.left = None
        self.parent_left = parent_left
        self.parent_right = parent_right
        self.spawnHeal()

    def spawnHeal(self):
        if random() < .14:
            self.heal = True
        else:
            self.heal = False

    def setChildren(self, left, right):
        self.left = left
        self.right = right

    def isChildOf(self, node):
        if not self.parent_left and not self.parent_right:
            return False
        if self.parent_left == node or self.parent_right == node:
            return True
        return (self.parent_left and self.parent_left.isChildOf(node)) or \
               (self.parent_right and self.parent_right.isChildOf(node))

    def getLeftNeigh(self):
        if self.parent_left and self.parent_left.left:
            return self.parent_left.left

    def getRightNeigh(self):
        if self.parent_right and self.parent_right.right:
            return self.parent_right.right


class Tree:
    def __init__(self):
        self.curNode = Node(0, None)
        self.topLevel = [self.curNode]
        self.score = 0
        for i in range(6):
            self.addLevel()

    def addLevel(self, parent=None):
        newTopLevel = []
        for n in self.topLevel:
            if n and (n.isChildOf(parent) if parent else True):
                lft, rg = self.createChildNodes(n)
                n.setChildren(lft, rg)
                newTopLevel.extend([lft, rg])
        self.topLevel = newTopLevel

    def createChildNodes(self, parent):
        parent_left_neigh = parent.getLeftNeigh()
        if parent_left_neigh and parent_left_neigh.right:
            parent_left_neigh.right.parent_right = parent
            if random() > .5:
                return parent_left_neigh.right, Node(parent_left=parent)
            else:
                return parent_left_neigh.right, None

        ch = choice(['right', 'left', 'both'])
        if ch == 'right':
            return Node(parent_right=parent), None
        elif ch == 'left':
            return None, Node(parent_left=parent)
        else:
            return Node(parent_right=parent), Node(parent_left=parent)

    def setCurNode(self, node):
        self.curNode = node
        if node:
            self.curNode.parent_left = None
            self.curNode.parent_right = None

    def go(self):
        if not self.curNode:
            return False, self.score
        self.score += 1
        self.addLevel(parent=self.curNode)
        return True, self.score

    def goRight(self):
        self.setCurNode(self.curNode.right)
        return self.go()

    def goLeft(self):
        self.setCurNode(self.curNode.left)
        return self.go()
